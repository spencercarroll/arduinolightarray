#!/bin/sh

#  move_to_library.sh
#  ArduinoLightArray
#
#  Created by Spencer Carroll on 9/9/14.
#  Copyright (c) 2014 Spencer Carroll. All rights reserved.

# A helpful script to move all the files into the Arduino Library as a flat structure. The Arduino library doesnt support heirarchical file and folder structures. I suggest to develop in the IDE of you choosing, execute this script and then run the Arduino's program complile. Alternatively consider using embedXcode.

#Change this to the path for your system.
DEST="/Applications/Arduino.app/Contents/Resources/Java/libraries/ArduinoLightArray"
find "ArduinoLightArray" -maxdepth 1 -type f -not -name '.git*' -not -name '*.sh' -not -name '*.ino' -not -name 'main.cpp' -exec rsync {} $DEST \;
find "ArduinoLightArray/light-array/Core" -maxdepth 1 -type f -exec rsync {} $DEST \;
mkdir -p $DEST/examples
cp ArduinoLightArray/main_sketch/main_sketch.ino $DEST/examples/TypicalUse.ino
