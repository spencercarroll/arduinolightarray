
#include <ArduinoPlatform.h>
#include <LightArray.h>
#include <Tlc5940.h>
#include <TrueRandom.h>
#include <Math.h>

void setup(){
    Tlc.init();
}


void loop(){
    srand(TrueRandom.random());//Arduino's random function isn't very random. We seed it with TrueRandom to fix that.
    Device &dev = *new Device(new ArduinoPlatform);
    smarray<ShowDescription const *>& showDescriptions = *(new ShowDescriptionCreator())->createShowDescriptions(dev);
    ShowRunner *runner  = new ShowRunner(showDescriptions, dev);
    runner->run();
}
