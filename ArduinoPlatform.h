//
//  ArduinoPlatform.h
//  TestEmbedXcode
//
//  Created by Spencer Carroll on 8/8/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __TestEmbedXcode__ArduinoPlatform__
#define __TestEmbedXcode__ArduinoPlatform__

#include <iostream>
#include "Platform.h"


class ArduinoPlatform :public Platform{
    
public:
    virtual void setChannel(int channel, LAFloat value);
    virtual LAFloat getChannel(int channel);
    virtual void clearChannel(int channel);
    virtual void clearAll();
    virtual void update();
    virtual void pause(unsigned long millis);
    virtual const int numChannels();
};

#endif /* defined(__TestEmbedXcode__ArduinoPlatform__) */
