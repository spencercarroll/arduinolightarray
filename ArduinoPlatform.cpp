//
//  ArduinoPlatform.cpp
//  TestEmbedXcode
//
//  Created by Spencer Carroll on 8/8/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "ArduinoPlatform.h"
#include "Tlc5940.h"
#include "Arduino.h"

static const int maxBrightness = 4095;

//Change this according to how you wired your LightArray. For example in this case the tlc channel 23 controls what looks like first light to the viewer. Channel 9 looks like the last.
int lightMap[32] = {23, 24, 25, 26, 27, 28, 31, 30, 29, 18, 17, 16,  15,  14,  13,  2,  1,  0,  3,  4,  10,  11,  12,  22,  21,  20,  19,  7,  6,  5,  8,  9};

void ArduinoPlatform::setChannel(int channel, LAFloat value){
    Tlc.set(lightMap[channel], (int)(value*maxBrightness)%(maxBrightness+1));
}
LAFloat ArduinoPlatform::getChannel(int channel){
    int intVal = Tlc.get(lightMap[channel]);
    LAFloat retVal = ((LAFloat)intVal)/maxBrightness;
    return retVal;
}
void ArduinoPlatform::clearChannel(int channel){
    Tlc.set(lightMap[channel], 0.0);
}
void ArduinoPlatform::clearAll(){
    Tlc.clear();
}
void ArduinoPlatform::update(){
    Tlc.update();
}
void ArduinoPlatform::pause(unsigned long millis){
    delay(millis);
}
const int ArduinoPlatform::numChannels(){
    return 32;//change to whatever yours does.
}