# ArduinoLightArray README #

#### About ####
ArduinoLightArray is the Arduino implementation of [LightArray](https://bitbucket.org/spencercarroll/light-array) for creating light shows using an array of individually dimmable lights. ArduinoLightArray uses the [TLC5940 chips](http://playground.arduino.cc/learning/TLC5940) to make controlling dimmable lights easy. If you are interested in building an ArduinoLightArray frame like the one below or just want more information take a look at the article [here](http://spencer-carroll.com/the-lightarray-project/). If you want to work on LightArray without an Arduino take a look at the iOS implementation found [here](https://bitbucket.org/spencercarroll/ioslightarray). 

![SpiralLit.jpg](https://bitbucket.org/repo/KGLrAy/images/1095897222-SpiralLit.jpg)

*If the image isn't enough checkout the [youtube video](https://www.youtube.com/watch?v=GJcx7Aw4Lbs)*
#### Getting Started ####
To get started with ArduinoLightArray go to the [download page](https://bitbucket.org/spencercarroll/arduinolightarray/downloads) and download the library, ArduinoLightArray.zip. You will also need to install the [Tlc5940 library](https://code.google.com/p/tlc5940arduino/). If you're unfamiliar with installing libraries on Arduino consult this [guide](http://arduino.cc/en/Guide/Libraries). 

#### Writing Your Own Code ####
If you are interested in writing your own shows for LightArray or making significant modifications I suggest cloning both this repository and the [iOS repository](http://spencer-carroll.com/the-lightarray-project/). Use the iOS implementation to develop your ideas and update the LightArray core implementation. When you're ready to run the code on Arduino just merge the updated LightArray code into your ArduinoLightArray code. The reason for this approach is simple. Debugging on Arduino is difficult. There is no debugger available for Arduino and when something goes wrong the lights will just shut off. Print statements will be your only friend. If you can't access Xcode to run the iOS implementation consider using the TestPlatform in light-array/Test to help debug or build your own simulation environment like iOS. Feel free to send me a message if you build your own.

Even if you use iOS to develop ideas you will probably still need a few code-upload-debug cycles to iron out any bugs or performance issues. To make this more convenient I suggest using an IDE of your choosing in tandem with the Arduino supplied IDE. The Arduino IDE isn't suited for large projects like this but does a great job of getting the code onto the Arduino. The following workflow works great. 

1. Develop on your IDE like Xcode, eclipse, etc. 
2. Run the move_to_library.sh script that moves all of the necessary files into the ArduinoLightArray Library.
3. Compile and upload code using the Arduino IDE. Use the main_sketch.ino file as the main sketch.
 
*If your not a fan of this workflow checkout [embedXcode](http://embedxcode.weebly.com).*

I would love for others to contribute to LightArray. You could add shows, help me with any c++ bugs ( c++ is not my forte) or add new features like adjusting the light brightness based on photoresistor readings. Message me if you are interested or email me at  s c a r r o l l 1 5 7 8 @ g m a i l . c o m. 


#### ArduinoLightArray Specific Code ####

The only Arduino specific code worth mentioning is the use of TrueRandom. The Arduino random function is anything but random. To provide randomness TrueRandom is used to seed the Math.h random function srand(int seed). 

No modifications were made to TrueRandom. The project home, license (currently GNU LGPL 3.0) and source for TrueRandom may be found [here](https://code.google.com/p/tinkerit/wiki/TrueRandom).